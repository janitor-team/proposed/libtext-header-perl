Source: libtext-header-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends-Indep: debhelper,
                     perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtext-header-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtext-header-perl.git
Homepage: https://metacpan.org/release/Text-Header
Build-Depends: debhelper-compat (= 12)

Package: libtext-header-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Multi-Arch: foreign
Description: RFC 822/2068 header and unheader functions
 Text::Header provides two new functions, header and unheader,
 which provide general-purpose RFC 822 header construction and parsing.
 They do not provide any intelligent defaults of HTTP-specific methods.
 They are simply aimed at providing an easy means to address the
 mechanics of header parsing.
 .
 The output style is designed to mimic CGI.pm and HTTP::Headers,
 so that users familiar with these interfaces will feel at home with
 these functions.
